### My Reply
Second Project of Supervised Learning to create Marketing model for Personal Loan Distribution.
##### Anish Mandal, Jan 24, 7:43 PM

-----

### Moderator
There are 52 entries in Experience where the total experience is in negative. We can remove these entries but we'll eliminate 1.04% data from total. We can impute these entries with mean, median, mode or any other logical entry. Based on the heat map we can see that there is high correlation between Age and Experience and moderate correlation between Income and CCAvg. For our dependent variable 'Personal Loan' it's having moderate correlation with Income, CCAvg, Mortgage and CD Account. The Correlation between Personal Loan and ZIP Code, Security Account, Online, CreditCard, EL_2, EL_3 is poor, we will perform CHI-Square test to identify if the categorical variables are dependent on each other, If not we will drop them from study. you have to find the inference like this. Delete comment: Based on the heat map we can see that there is high correlation between Age and Experience and moderate correlation between Income and CCAvg. For our dependent variable 'Personal Loan' it's having moderate correlation with Income, CCAvg, Mortgage and CD Account. The Correlation between Personal Loan and ZIP Code, Security Account, Online, CreditCard, EL_2, EL_3 is poor, we will perform CHI-Square test to identify if the categorical variables are dependent on each other, If not we will drop them from study. you have to find the inference like this. Keep up your Good Work!
###### Moderator, Feb 02, 3:56 PM